/*
 * xrdesktop
 * Copyright 2019 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#include <glib.h>

#include <xrd.h>

static void
_assert_uint_not_0 (GSettings *settings, gchar *key, gpointer data)
{
  (void) data;
  guint result = g_settings_get_uint (settings, key);
  g_assert (result != 0);
}
static void
_assert_double_not_0 (GSettings *settings, gchar *key, gpointer data)
{
  (void) data;
  double result = g_settings_get_double (settings, key);
  g_assert (result != 0);
}

int
main ()
{
  if (!xrd_settings_is_schema_installed ())
    {
      g_print ("org.xrdestkop gsettings schema is not installed!\n");
      return 1;
    }

  xrd_settings_connect_and_apply (G_CALLBACK (_assert_uint_not_0),
                                  "input-poll-rate-ms", NULL);
  xrd_settings_connect_and_apply (G_CALLBACK (_assert_double_not_0),
                                  "grab-window-threshold", NULL);

  return 0;
}
